import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  msg: string;
  num: number;
  constructor(private service: DataService) { this.num = 0 }

  ngOnInit() { }

  FormValidation(UserData: any) {
    if (UserData.uname == "" || UserData.email == "" ||
      UserData.MobNo == "" || UserData.dob == "" ||
      UserData.pwd == "") {
      this.msg = "All fields are compulsory!!";
    }
    else {
      let resultstate = this.service.Signup(UserData);
      resultstate.subscribe((data: any) => {
        if (data != isNaN) {
          this.msg = "Successfully Signed Up!!";
          alert("Successfully Signed Up!!, Thank you!!")
          this.num = 1;
        }
        else {
          console.log(data);
          this.msg = "Somthing went Wrong!!";
          this.num = 0;
        }
      });
    }
  }
}