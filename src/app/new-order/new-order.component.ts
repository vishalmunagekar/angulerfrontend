import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit {
  User: any;
  Orders: any
  Pizzas: any;
  totalamt: number;
  pizzaqty: number;
  addrid: number;
  Ostatus : boolean;
  constructor(public AuthService: AuthService,
    public DataService: DataService) { this.Ostatus = true;
    
      this.totalamt = sessionStorage.totalamt;
      this.pizzaqty = sessionStorage.pizaaqty;
    }

  ngOnInit() {
    let getUser = this.DataService.getUserById(sessionStorage.id);
    getUser.subscribe((result: any) => {
      this.User = result;
    });
    this.Pizzas = JSON.parse(sessionStorage.PizzaIds);

  }

  placeOrder(Object: any) {
    if (Object.addrid == 0) {
      alert("Pls select Your Address");
    }
    else {
     // console.log(this.totalamt);
      //console.log(this.Orders);
      this.Pizzas[0].pizzaqty = sessionStorage.pizaaqty;
      this.Pizzas[0].totalamt = sessionStorage.totalamt;
      this.Pizzas[0].addrid = Object.addrid;
     // console.log(this.Pizzas);
      sessionStorage.setItem("Orders", JSON.stringify(this.Pizzas));
      let obj = this.DataService.addOrders(this.Pizzas, sessionStorage.id);
      obj.subscribe((result: any) => {
        if (result == 1) {
          alert("Order Succsessfully placed...!!!");
          this.Ostatus = false;
          sessionStorage.removeItem("PizzaIds");
        } else {
          alert("Somthing went wrong..!!!");
        }
      });
    }
  }
}