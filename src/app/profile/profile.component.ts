import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  UserProfileData: any
  id : any
  constructor(public AuthService : AuthService,
              public DataService : DataService) {
              }

  ngOnInit() {
    this.id =  sessionStorage.getItem("id");
    let FindUser = this.DataService.getUserById(this.id);
    FindUser.subscribe((result: any)=>{
      if(result.length != 0)
      {
        this.UserProfileData = result;
      }
    });
  }
  
}