import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  AllOrders: any
  VENDOR : any
  id: any
  constructor(public AuthService: AuthService,
    public router: Router,
    public DataService: DataService) {
  }
  ngOnInit() {
    if (sessionStorage.role != "VENDOR") {
      this.router.navigate(["Home"]);
    }
    this.id = sessionStorage.getItem("id");
    let User = this.DataService.getUserById(this.id);
    User.subscribe((result: any) => {
      this.VENDOR = result;
    });
    let Orders = this.DataService.getAllOrders();
    Orders.subscribe((result: any) => {
      this.AllOrders = result;
    });
  }

  getdetails(OrderId: any)
  {
    //console.log(OrderId);
    sessionStorage.setItem("OrderId", OrderId);
    this.router.navigate(['OrderDetails']);
  }

}
