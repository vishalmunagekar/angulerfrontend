import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  orderDetails: any;
  OrderId: any;
  constructor(public AuthService: AuthService,
    public router: Router,
    public DataService: DataService) {
  }

  updateOrderStatusDelivered() {
    this.DataService.updateOrderStatusDelivered(this.OrderId).subscribe((result) => {
      if (result == 1) {
        this.OrderId = sessionStorage.getItem("OrderId");
        let Odetails = this.DataService.getOrdersDetails(this.OrderId);
        Odetails.subscribe((result: any) => {
          this.orderDetails = result;
        });
        alert("Order Status Updated to delivered..!!");
      } else {
        alert("Somthing went Wrong..!!");
      }
    });
  }

  ngOnInit() {
    if (sessionStorage.role != "VENDOR") {
      this.router.navigate(["Home"]);
    }
    this.OrderId = sessionStorage.getItem("OrderId");
    let Odetails = this.DataService.getOrdersDetails(this.OrderId);
    Odetails.subscribe((result: any) => {
      this.orderDetails = result;
    });
  }
}
