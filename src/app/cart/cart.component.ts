import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  Pizzas: any
  constructor(public AuthService: AuthService,
    public DataService: DataService) {
  }

  ngOnInit() {
  }

}
