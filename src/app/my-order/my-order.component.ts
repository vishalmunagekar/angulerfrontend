import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.css']
})
export class MyOrderComponent implements OnInit {
  UsersOrders: any
  id: any
  constructor(public AuthService: AuthService,
    public DataService: DataService) {
  }


  ngOnInit() {
    this.id = sessionStorage.getItem("id");
    let FindUser = this.DataService.getUserById(this.id);
    FindUser.subscribe((result: any) => {
      this.UsersOrders = result;
    });
  }

  updateOrderStatusCanceled(OrderId) {
    this.DataService.updateOrderStatusCanceled(OrderId).subscribe((result) => {
      if (result == 1) {
        this.id = sessionStorage.getItem("id");
        let FindUser = this.DataService.getUserById(this.id);
        FindUser.subscribe((result: any) => {
          this.UsersOrders = result;
        });
        alert("Order Status Updated to delivered..!!");
      } else {
        alert("Somthing went Wrong..!!");
      }
    });
  }
}
