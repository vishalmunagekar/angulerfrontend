import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user;
  msg;
  constructor(public Auth_service: AuthService,
    public Data_service: DataService,
    public router: Router) { }

  ngOnInit() {
  }

  FormValidation(credentials: any) {
   // console.log(credentials)
    if (credentials.email == "" || credentials.pass == "") {
      this.msg = "Username/Password is required!!";
    }
    else {
      this.Login(credentials);
    }

  }

  Login(credentials: any) {
    let isLoggedIn = false;
   // console.log(credentials);
    let FindUser = this.Data_service.LoginData(credentials);
    FindUser.subscribe((result: any) => {
     // console.log(result);
      if(result == null){
        this.msg = " Username / Password is wrong!!";
        return;
      }
     // console.log(result.length);
      this.user = result;
     // console.log(this.user);
      isLoggedIn = this.Auth_service.Login(credentials, this.user);
      if (isLoggedIn) {
        this.msg = "";
        if(this.user.role == "USER"){
          sessionStorage.role = "USER";
          this.router.navigate(['Home']);
        }
        else
        {
          sessionStorage.role = "VENDOR";
          this.router.navigate(['AdminHome']);
        }
        
      }
      else {
        this.msg = " Username / Password is wrong!!";
      }
    });
  }
}
