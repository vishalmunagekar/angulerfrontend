import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { DataService } from './data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  id :any
  constructor(public Auth_service: AuthService,
    public Dataservice: DataService,
    public router: Router) {

      if(sessionStorage.id==null || sessionStorage.id==undefined )
      {

        sessionStorage.setItem("id", "0");
        sessionStorage.setItem("role", "USER");
      }
      this.id = sessionStorage.getItem("id");
      console.log(this.id);
  }
  checkRole()
  {
    return (sessionStorage.role == "VENDOR");
  }
  isLogin()
  {
    return (sessionStorage.id == "0");
  }

  Logout() {
    this.Auth_service.Logout();
    window.location.reload();
  }

  ngOnInit()
  {
    this.router.navigate(['Home'])
  }
}