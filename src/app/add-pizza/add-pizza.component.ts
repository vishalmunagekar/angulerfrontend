import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-pizza',
  templateUrl: './add-pizza.component.html',
  styleUrls: ['./add-pizza.component.css']
})
export class AddPizzaComponent implements OnInit {
  msg: any;
  image: any;
  imgUrl:any;
  constructor(private service: DataService,
    public router: Router) { }

    onSelect(event) {
    this.image = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.image);
    reader.onload = (_event) => { 
      this.imgUrl = reader.result; 
    }
  }

  addPizza(Pizza: any) {
    const formdata = new FormData();
    formdata.append("pname", Pizza.pname);
    formdata.append("category", Pizza.category);
    formdata.append("image", this.image);
    formdata.append("description", Pizza.description);
    formdata.append("status", Pizza.status);
    formdata.append("size", Pizza.size);
    formdata.append("price", Pizza.price);
    let resultstate = this.service.addPizza(formdata);
    resultstate.subscribe((result: any) => {
      if (result == 1) {
        alert("Pizza Added Successfully!!");
      }
      else {
        alert("Somthing went wrong...!!!");
      }
    });
  }

  ngOnInit() {
    if (sessionStorage.role != "VENDOR") {
      this.router.navigate(["Home"]);
    }
  }
}