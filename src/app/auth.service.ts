import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MD5, enc } from "crypto-js";
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
  UserData :any;
  
  constructor(public router: Router,
    public AuthService : AuthService,
    public DataService : DataService) {
    let StatusOfUpdate = this.DataService.getUserById(sessionStorage.id);
    StatusOfUpdate.subscribe((result:any)=>{
        this.UserData = result;
    });
   }

  isLoggedIn() {
    return (sessionStorage.getItem("id") == this.UserData.id);
  }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    if (this.isLoggedIn()) {
      return true;
    }
    else {
      this.router.navigate(['Login']);
      return false;
    }
  }
  Login(credentials: { email: any; pass: any }, result_data: { id: any; email: any; pass: any; }) {
    console.log(credentials);
    console.log(result_data);
    //const crypto_pwd = MD5(credentials.pass);
    //console.log(crypto_pwd);
    //console.log(result_data.pass);
    if (credentials.email == result_data.email &&
      credentials.pass == result_data.pass)               //crypto_pwd   == result_data.pass)
    {
      this.UserData = result_data;
      sessionStorage.setItem("id", result_data.id);
      return true;
    }
    else {
      return false;
    }
  }
  Logout() {
    sessionStorage.setItem("id", "0");
    sessionStorage.clear();
  }

}
