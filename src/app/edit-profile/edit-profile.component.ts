import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  UserProfileData: any
  id : any
  msg;
  constructor(public routes: ActivatedRoute,
    public DataService: DataService,
    public router: Router) {
    this.msg = "";
  }

  ngOnInit() {
    this.id =  sessionStorage.getItem("id");
    let FindUser = this.DataService.getUserById(this.id);
    FindUser.subscribe((result: any)=>{
      if(result.length != 0)
      {
        this.UserProfileData = result;
      }
    });
  }
  UpdateUserData(UserData)
  {
    let StatusOfUpdate = this.DataService.UpdateUserData(UserData);
    StatusOfUpdate.subscribe((result:any)=>{
        if(result == 1)
        {
          alert("Profile Updated Successfully...!!")
          this.router.navigate(['Profile']);
        }
        else
        {
          this.msg = "Something went wrong!";
        }
    });
  }
}
