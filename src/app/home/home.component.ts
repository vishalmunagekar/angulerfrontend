import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pizzas: any;
  carts: any[] = [];
  totalAmt: any = 0;
  totalQty: any = 0;
  IsCartEmpty: boolean = true;
  size1: string = "PERSONAL";
  size2: string = "MEDIUM";
  size3: string = "FAMILY";
  constructor(public AuthService: AuthService,
    public DataService: DataService) {
    if (sessionStorage.PizzaIds == undefined)
      sessionStorage.setItem("PizzaIds", JSON.stringify(this.carts));
    else
      this.IsCartEmpty = false
      this.carts = JSON.parse(sessionStorage.getItem("PizzaIds"));
      if(this.carts.length == 0)
      this.IsCartEmpty = true;
      this.getTotal();
      //console.log("Inside ctor");
  }
  ngOnInit() {
    let getPizza = this.DataService.getAllPizzas();
    getPizza.subscribe((result: any) => {
      this.pizzas = result;      
    });
    this.carts = JSON.parse(sessionStorage.getItem("PizzaIds"));
    //console.log(this.IsCartEmpty);
    //console.log(this.carts.length);
    this.getTotal();
    //console.log("ngOnInit");
  }

  getVegPizzas()
  {
    let getPizza = this.DataService.getVegPizzas();
    getPizza.subscribe((result: any)=>{
        this.pizzas = result;
    });
  }

  getNonVegPizzas()
  {
    let getPizza = this.DataService.getNonVegPizzas();
    getPizza.subscribe((result: any)=>{
        this.pizzas = result;
    });
  }


  pushToCart(pizza) {
   // console.log(pizza.pizzaid);
   // console.log(this.carts.length);
    let flag: boolean = false;
    pizza.totalprice = pizza.price;
    if (this.carts.length > 0) {
      for (let i = 0; i < this.carts.length; i++) {

        if (this.carts[i].pizzaid == pizza.pizzaid) {
          this.carts[i].qty += 1;
          this.carts[i].totalprice = pizza.price * this.carts[i].qty;
          this.getTotal();
          sessionStorage.setItem("PizzaIds", JSON.stringify(this.carts)); //imp step
          return;
        }
      }
    }
    pizza.qty = 1;
    this.carts.push(pizza);
    sessionStorage.setItem("PizzaIds", JSON.stringify(this.carts));
    this.IsCartEmpty = false;
    this.getTotal();
  }

  popToCart(pizza) {
    if (this.carts.length > 0) {
      for (let i = 0; i < this.carts.length; i++) {

        if (this.carts[i].pizzaid == pizza.pizzaid) {
          if (this.carts[i].qty > 1) {
            this.carts[i].qty -= 1;
            this.carts[i].totalprice = pizza.price * this.carts[i].qty;
          }
          else {
            this.carts[i].qty = 0;
            this.carts.splice(i, 1);
          }
        }
      }
      sessionStorage.setItem("PizzaIds", JSON.stringify(this.carts));
      if (sessionStorage.PizzaIds.length == 2) { this.IsCartEmpty = true }
    }
    this.getTotal();
  }

  getTotal() {
    this.totalAmt = 0;
    this.totalQty =0;
    if (this.carts.length > 0) {
      for (let i = 0; i < this.carts.length; i++) {
          this.totalAmt += this.carts[i].totalprice;
          this.totalQty += this.carts[i].qty;
          sessionStorage.setItem("totalamt", this.totalAmt);
          sessionStorage.setItem("pizaaqty", this.totalQty);
      }
    }
  }
  isLogin()
  {
    return (sessionStorage.id == "0")
  }


  getBySize(size:string)
  {
     let pizzasBySize = this.DataService.getPizzaBySize(size).subscribe((result)=>{
       this.pizzas = result;
     });
  }

  getByName(name:string)
  {
   // console.log(name);
     let pizzasBySize = this.DataService.getPizzaByName(name).subscribe((result)=>{
       this.pizzas = result;
     });
  }
}