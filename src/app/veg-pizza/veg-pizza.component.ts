import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-veg-pizza',
  templateUrl: './veg-pizza.component.html',
  styleUrls: ['./veg-pizza.component.css']
})
export class VegPizzaComponent implements OnInit {
  pizzas : any;
  constructor(public AuthService : AuthService,
    public DataService : DataService) {
    }

  ngOnInit() {
    let getPizza = this.DataService.getVegPizzas();
    getPizza.subscribe((result: any)=>{
        this.pizzas = result;
    });
  }

}
