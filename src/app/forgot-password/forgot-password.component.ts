import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  result: any
  constructor(public Auth_service: AuthService,
    public Data_service: DataService) { }

  forgotPassword(email: any) {
    //console.log(email);
    if (email == 0) {
        alert("Please Enter Your Email...!!")
    } else{
      this.Data_service.forgotUserPassword(email).subscribe((result: any) => {
        this.result = result;
       // console.log(this.result);
        if (this.result == 1)
          alert("Password has been successefully changed!");
      });
    }
  }
  ngOnInit() {
  }

}
