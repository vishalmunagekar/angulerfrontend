import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonVegPizzaComponent } from './non-veg-pizza.component';

describe('NonVegPizzaComponent', () => {
  let component: NonVegPizzaComponent;
  let fixture: ComponentFixture<NonVegPizzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonVegPizzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonVegPizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
