import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-non-veg-pizza',
  templateUrl: './non-veg-pizza.component.html',
  styleUrls: ['./non-veg-pizza.component.css']
})
export class NonVegPizzaComponent implements OnInit {
  pizzas : any;
  constructor(public AuthService : AuthService,
    public DataService : DataService) {
    }

  ngOnInit() {
    let getPizza = this.DataService.getNonVegPizzas();
    getPizza.subscribe((result: any)=>{
        this.pizzas = result;
    });
  }
}
