import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { DataService } from './data.service';
import { AppRoutingModule } from './app-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { VegPizzaComponent } from './veg-pizza/veg-pizza.component';
import { NonVegPizzaComponent } from './non-veg-pizza/non-veg-pizza.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { MyOrderComponent } from './my-order/my-order.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { AddPizzaComponent } from './add-pizza/add-pizza.component';
import { CartComponent } from './cart/cart.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NewOrderComponent } from './new-order/new-order.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { EditPizzaStatusComponent } from './edit-pizza-status/edit-pizza-status.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    ProfileComponent,
    EditProfileComponent,
    NotFoundComponent,
    VegPizzaComponent,
    NonVegPizzaComponent,
    LoginComponent,
    SignupComponent,
    ResetPasswordComponent,
    AdminHomeComponent,
    MyOrderComponent,
    OrderDetailsComponent,
    AddPizzaComponent,
    CartComponent,
    ForgotPasswordComponent,
    NewOrderComponent,
    AddAddressComponent,
    EditPizzaStatusComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot([
      {path : "" , component : HomeComponent},  
      {path : "Home" , component : HomeComponent},
      {path : "About" , component : AboutComponent},
      {path : "Login" , component : LoginComponent},
      {path : "Signup" , component : SignupComponent},
      {path : "Profile" , component : ProfileComponent ,canActivate: [AuthService]}, 
      {path : "EditProfile" , component : EditProfileComponent ,canActivate: [AuthService]},
      {path : "Contact" , component : ContactComponent},
      {path : "NewOrder" , component : NewOrderComponent, canActivate: [AuthService]},
      {path : "ForgotPassword" , component : ForgotPasswordComponent},
      {path : "EditPizzaStatus" , component : EditPizzaStatusComponent},
      {path : "AddAddress" , component : AddAddressComponent, canActivate: [AuthService]},
      {path : "AddPizza" , component : AddPizzaComponent},
      {path : "OrderDetails" , component : OrderDetailsComponent, canActivate: [AuthService]},
      {path : "MyOrder" , component : MyOrderComponent, canActivate: [AuthService]},  
      {path : "AdminHome" , component : AdminHomeComponent, canActivate: [AuthService]},
      {path : "**" , component : NotFoundComponent}
    ]),
  ],
  providers: [HttpClientModule, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }



//{path : "NonVegPizza" , component : NonVegPizzaComponent},
//{path : "VegPizza" , component : VegPizzaComponent},
//{path : "Cart" , component : CartComponent, canActivate: [AuthService]},