import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {
  msg:any;
  constructor(public Auth_service: AuthService,
    public Data_service: DataService,
    public router: Router ) { }

  ngOnInit() {
  }

  AddAddress(Address)
  {
      let address = this.Data_service.addAddress(sessionStorage.id, Address).subscribe((result)=> {
          if(result == 1){
            this.msg = "Address Successfully Added..!!";
            alert("Address Successfully Added..!!");
          } else{
            this.msg = "Somthing went wrong..!!";
            alert("Somthing went wrong..!!");
          }
      });
  }
}
