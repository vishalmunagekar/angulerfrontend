import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-pizza-status',
  templateUrl: './edit-pizza-status.component.html',
  styleUrls: ['./edit-pizza-status.component.css']
})
export class EditPizzaStatusComponent implements OnInit {
  pizzas: any;
  constructor(public AuthService: AuthService,
    public router: Router,
    public DataService: DataService) {
  }

  changeStatus(pid: any, status: string) {
    console.log(pid, status);
    if (status == "AVAILABLE") {
        this.DataService.changeStatusNotavailable(pid).subscribe((result) => {
        let getPizza = this.DataService.getAllPizzas();
        getPizza.subscribe((result: any) => {
          this.pizzas = result;
        });
      });
    } else {
        this.DataService.changeStatusAvailable(pid).subscribe((result) => {
        let getPizza = this.DataService.getAllPizzas();
        getPizza.subscribe((result: any) => {
          this.pizzas = result;
        });
      });
    }
  }

  ngOnInit() {
    let getPizza = this.DataService.getAllPizzas();
    getPizza.subscribe((result: any) => {
      this.pizzas = result;
    });
  }
}
