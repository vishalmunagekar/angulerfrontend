import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPizzaStatusComponent } from './edit-pizza-status.component';

describe('EditPizzaStatusComponent', () => {
  let component: EditPizzaStatusComponent;
  let fixture: ComponentFixture<EditPizzaStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPizzaStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPizzaStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
