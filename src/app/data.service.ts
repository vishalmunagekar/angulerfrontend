import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public http: HttpClient) { }
  LoginData(credentials: { email: any; pass: any; }) {
    return this.http.post("http://localhost:8080/PizzaHot/User/login",credentials);//15.206.88.236
  }

  getUserById(id : any)
  {
     return this.http.get("http://localhost:8080/PizzaHot/User/" + id);
  }
  getAllPizzas()
  {
    return this.http.get("http://localhost:8080/PizzaHot/Pizza" );
  }
  Signup(UserData: any)
  {
    return this.http.post("http://localhost:8080/PizzaHot/User/signup", UserData);
  }

  getAllOrders()
  {
    return this.http.get("http://localhost:8080/PizzaHot/Orders");
  }

  getOrdersDetails(OrderId: string)
  {
    return this.http.get("http://localhost:8080/PizzaHot/Orders/" + OrderId);
  }

  getVegPizzas()
  {
    return this.http.get("http://localhost:8080/PizzaHot/Pizza/Veg");
  }

  getNonVegPizzas()
  {
    return this.http.get("http://localhost:8080/PizzaHot/Pizza/NonVeg");
  }

  forgotUserPassword(email: any)
  {
    return this.http.post("http://localhost:8080/PizzaHot/User/forgotpass", email);
  }

  addOrders(data: any, uid)
  {
    return this.http.post("http://localhost:8080/PizzaHot/Orders/add/"+uid , data);
  }

  addAddress(uid: any, address: any){
    return this.http.post("http://localhost:8080/PizzaHot/User/addaddress/"+uid , address);
  }

  updateOrderStatusDelivered(oid: string){
    return this.http.get("http://localhost:8080/PizzaHot/Orders/delivered/"+oid);
  }

  updateOrderStatusCanceled(oid: string){
    return this.http.get("http://localhost:8080/PizzaHot/Orders/canceled/"+oid);
  }

  addPizza(pizza){
    return this.http.post("http://localhost:8080/PizzaHot/Pizza/add", pizza);
  }

  getPizzaBySize(size: string) {
    return this.http.get("http://localhost:8080/PizzaHot/Pizza/BySize/"+ size);
  }

  getPizzaByName(name:string){
    return this.http.get("http://localhost:8080/PizzaHot/Pizza/ByName/"+ name);
  }

  UpdateUserData(UserData: any)
  {
      return this.http.put("http://localhost:8080/PizzaHot/User/update", UserData);
  }

  changeStatusAvailable(pid: any){
    return this.http.get("http://localhost:8080/PizzaHot/Pizza/Available/"+pid);
  }

	changeStatusNotavailable(pid: any){
    return this.http.get("http://localhost:8080/PizzaHot/Pizza/Notavailable/"+pid);
  }
}